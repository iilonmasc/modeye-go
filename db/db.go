package db

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const MongoDatabase = "modeye"

type Pack struct {
	Name       string //mcpack-*Pack.Version
	Version    string //v20220924
	State      string // c = current, f = future, a = archived
	Previous   string //mcpack-v20220917
	Next       string //mcpack-v20221008
	Mods       []string
	Changelogs []string
}
type Changelog struct {
	Name   string // *Pack.Name-*OldMod.Name-*OldMod.Version-*NewMod.Version
	Pack   string
	OldMod string
	NewMod string
}
type Mod struct {
	docName     string //Ars Nouveau 2.7.8
	Name        string //Ars Nouveau
	Version     string //2.7.8
	Homepage    string //http://example.com/ars_nouveau
	DownloadURL string //http://example.com/ars_nouveau/download/2.7.8
}

func CheckError(e error) {
	if e != nil {
		fmt.Println(e)
	}
}
func CreateClient() *mongo.Client {

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")

	// Connect to MongoDB
	client, e := mongo.Connect(context.TODO(), clientOptions)
	CheckError(e)

	// Check the connection
	e = client.Ping(context.TODO(), nil)
	CheckError(e)
	return client

}

func updateRefs(current Pack, packCollection *mongo.Collection) {
	var filter bson.D
	var err error
	if current.Previous != "" {
		var previous Pack
		fmt.Printf("Checking for previous versions with name %s\n", current.Previous)
		filter = bson.D{{"name", current.Previous}}
		err = packCollection.FindOne(context.TODO(), filter).Decode(&previous)
		CheckError(err)
		fmt.Printf("Found %v during check for previous versions\n", previous)
		previousUpdate := bson.D{
			{"$set", bson.D{
				{"next", current.Name},
			}},
		}
		_, err = packCollection.UpdateOne(context.TODO(), filter, previousUpdate)
		CheckError(err)
	}
	filter = bson.D{{"name", current.Name}}
	currentUpdate := bson.D{
		{"$set", bson.D{
			{"previous", current.Previous},
			{"state", current.State},
		}},
	}
	_, err = packCollection.UpdateOne(context.TODO(), filter, currentUpdate)
	CheckError(err)
}
func CheckModPacks(client *mongo.Client) {
	packCollection := client.Database(MongoDatabase).Collection("packs")
	cursor, err := packCollection.Find(context.TODO(), bson.D{})
	CheckError(err)
	var results []Pack
	if err = cursor.All(context.TODO(), &results); err != nil {
		CheckError(err)
	}
	for _, result := range results {
		filter := bson.D{{"name", result.Previous}}
		var previous Pack
		err = packCollection.FindOne(context.TODO(), filter).Decode(&previous)
		CheckError(err)
		if len(result.Mods) == 0 {
			// add previous mods to this pack
			result.Mods = previous.Mods

		} else if len(previous.Mods) > 0 {
			for i := range previous.Mods {
				previousMod := previous.Mods[i]
				foundMod := false
				for j := range result.Mods {
					resultMod := result.Mods[j]
					if resultMod.Name == previousMod.Name {
						foundMod = true
						if resultMod.Version != previousMod.Version {

							// check if changelog exists
							changelogExists := false
							for _, cl := range result.Changelogs {
								if cl.Name == result.Name+"-"+previousMod.Name+"-"+previousMod.Version+"-"+resultMod.Version {
									changelogExists = true
								}
							}
							if !changelogExists {
								// generate a changelog
								changelog := Changelog{
									Name:   result.Name + "-" + previousMod.Name + "-" + previousMod.Version + "-" + resultMod.Version,
									Pack:   result,
									OldMod: previousMod,
									NewMod: resultMod,
								}
								result.Changelogs = append(result.Changelogs, changelog)
								// TODO: only append reference name to modpack array

							}
						}
					}

				}
				if !foundMod {
					result.Mods = append(result.Mods, previousMod)
				}
			}
		}
		filter = bson.D{{"name", result.Name}}
		resultUpdate := bson.D{
			{"$set", bson.D{
				{"mods", result.Mods},
				{"changelogs", result.Changelogs},
			}},
		}
		_, err = packCollection.UpdateOne(context.TODO(), filter, resultUpdate)
		CheckError(err)

	}
}
func updateCheck(current Pack, mod Mod, packCollection *mongo.Collection) {
	var filter bson.D
	var err error
	var previous Pack
	fmt.Printf("Checking for previous versions with name %s\n", current.Previous)
	filter = bson.D{{"name", current.Previous}}
	err = packCollection.FindOne(context.TODO(), filter).Decode(&previous)
	CheckError(err)
	fmt.Printf("Found %v during check for previous versions\n", previous)
	// check for version differences if current pack has mods
	if len(current.Mods) == 0 {
		// add previous mods to this pack
		current.Mods = previous.Mods

	} else if len(previous.Mods) > 0 {
		for i := range previous.Mods {
			previousMod := previous.Mods[i]
			foundMod := false
			for j := range current.Mods {
				currentMod := current.Mods[j]
				if currentMod.Name == previousMod.Name && currentMod.Version != previousMod.Version {

					// generate a changelog
					changelog := Changelog{
						Name:   current.Name + "-" + previousMod.Name + "-" + previousMod.Version + "-" + currentMod.Version,
						Pack:   current,
						OldMod: previousMod,
						NewMod: currentMod,
					}
					current.Changelogs = append(current.Changelogs, changelog)
				}

			}
			if !foundMod {
				current.Mods = append(current.Mods, previousMod)
			}
		}
	}
	filter = bson.D{{"name", current.Name}}
	currentUpdate := bson.D{
		{"$set", bson.D{
			{"mods", current.Mods},
			{"changelogs", current.Changelogs},
		}},
	}
	_, err = packCollection.UpdateOne(context.TODO(), filter, currentUpdate)
	CheckError(err)
}

func CreateVersion(version Pack, client *mongo.Client) {
	// create Version
	// NEED:
	//   - version number
	//   - name of previous (if applicable)
	//
	// DO:
	//   - if previous: fetch mods from previous, update *Previous.Next field with new version as *Version
	//   - else: no mods

	// check for given previous Version

	fmt.Printf("Got the following Version: %v\n", version)

	packCollection := client.Database(MongoDatabase).Collection("packs")
	var err error

	// check if current version already exists
	filter := bson.D{{"name", version.Name}}
	var current Pack
	err = packCollection.FindOne(context.TODO(), filter).Decode(&current)
	CheckError(err)
	if current.Name == "" {
		fmt.Printf("%s does not exist, creating it\n", version.Name)
		_, err = packCollection.InsertOne(context.TODO(), version)
		CheckError(err)
	}
	updateRefs(version, packCollection)
}
func updateHomepageURL(mod Mod, modCollection *mongo.Collection) {
	// check if a mod of this family exists with homepage
	filter := bson.D{{"name", mod.Name}, {"homepage", bson.D{{"$ne", ""}}}}
	var base Mod
	err := modCollection.FindOne(context.TODO(), filter).Decode(&base)
	CheckError(err)
	fmt.Printf("Searching for %s mods with homepage, Found %v\n", mod.Name, base)
	if (base != Mod{}) {
		filter = bson.D{{"name", mod.Name}, {"version", mod.Version}}
		currentUpdate := bson.D{
			{"$set", bson.D{
				{"homepage", base.Homepage},
			}},
		}
		_, err = modCollection.UpdateOne(context.TODO(), filter, currentUpdate)
		CheckError(err)
	}
}
func CreateMod(mod Mod, versionName string, client *mongo.Client) {
	// create Mod
	// NEED:
	//   - name
	//   - homepage
	//   - version number
	//   - download of specific version
	//   - name of modpack
	// DO:
	//   - if mod already in pack but different version: generate changelog, remove old mod, add new mod, attach changelog
	//   - else: add mod to version []*Mod
	// TODO: create docName and use only reference in Modpacks
	var err error
	var filter bson.D
	packCollection := client.Database(MongoDatabase).Collection("packs")
	modCollection := client.Database(MongoDatabase).Collection("mods")

	// check if exact mod in this version exists
	filter = bson.D{{"name", mod.Name}, {"version", mod.Version}}
	var current Mod
	err = modCollection.FindOne(context.TODO(), filter).Decode(&current)
	CheckError(err)
	// fmt.Printf("Searching for %s @ %s, Found %v\n", mod.Name, mod.Version, current)
	if (current == Mod{}) {
		_, err = modCollection.InsertOne(context.TODO(), mod)
		CheckError(err)
	} else if mod.Homepage == "" {
		updateHomepageURL(mod, modCollection)
	}

	// fetch modpack by versionName
	filter = bson.D{{"name", "mcpack-" + versionName}}
	var modPack Pack
	err = packCollection.FindOne(context.TODO(), filter).Decode(&modPack)
	CheckError(err)
	if err == nil {
		fmt.Printf("Found mod-referenced modpack %v\n", modPack)
		// check whether modpack contains mods of the same family
		if len(modPack.Mods) > 0 {
			fmt.Println("Modpack contains mods")
			foundModInPack := false
			for i := range modPack.Mods {
				currentMod := modPack.Mods[i]
				fmt.Printf("Current mod %v\nChecking against %v\n", currentMod, mod)
				if currentMod.Name == mod.Name {
					foundModInPack = true
					if currentMod.Version != mod.Version {
						fmt.Printf("Found version difference, correction version %s to %s\n", currentMod.Version, mod.Version)
						modPack.Mods[i] = mod
					} else {
						fmt.Printf("Exact version (%s) found\n", currentMod.Version)
					}
				}
			}
			if !foundModInPack {
				modPack.Mods = append(modPack.Mods, mod)
			}
		} else {
			modPack.Mods = append(modPack.Mods, mod)
		}
		filter = bson.D{{"name", modPack.Name}}
		updateSet := bson.D{
			{"$set", bson.D{
				{"mods", modPack.Mods},
			}},
		}
		_, err = packCollection.UpdateOne(context.TODO(), filter, updateSet)
		CheckError(err)
	}
}

/*
func something() {
	// get collection as ref
	collection := client.Database(MongoDatabase).Collection("mods")
	// insert
	john := Person{"John", 24}
	jane := Person{"Jane", 27}
	ben := Person{"Ben", 16}

	_, e := collection.InsertOne(context.TODO(), john)
	CheckError(e)

	persons := []interface{}{jane, ben}
	_, e = collection.InsertMany(context.TODO(), persons)
	CheckError(e)
	// update
	filter := bson.D{{"name", "John"}}

	update := bson.D{
		{"$set", bson.D{
			{"age", 26},
		}},
	}

	_, e = collection.UpdateOne(context.TODO(), filter, update)
	CheckError(e)
}
*/
