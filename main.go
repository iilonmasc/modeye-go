package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/iilonmasc/modeye-go/db"
	"go.mongodb.org/mongo-driver/mongo"
)

func handler(w http.ResponseWriter, r *http.Request) {
	var parkDeck, jsonString string
	w.Header().Set("Gotool", "modeye")
	w.Header().Set("Server", "gotools")
	// if r.Method == "POST" {
	// 	data, err := ioutil.ReadAll(r.Body)
	// 	if err == nil {
	// 		var deckMap map[string]json.RawMessage
	// 		err = json.Unmarshal(data, &deckMap)
	// 		newDeck := bytes.NewBuffer(deckMap["deck"]).String()
	// 		if err == nil && newDeck != "" {
	// 			db.WriteDBContent(newDeck)
	// 		}
	// 	}
	// }
	// if db.CheckForDBFile() {
	// 	parkDeck = db.ReadDBContent()
	// 	if parkDeck == "" {
	// 		parkDeck = "none"
	// 	}
	// } else {
	// 	parkDeck = "none"
	// }
	jsonString = "{\"park-deck\": \"" + parkDeck + "\"}"
	io.WriteString(w, jsonString)
}

func main() {
	if os.Getenv("MODEYE_INTERACTIVE") == "true" {
		client := db.CreateClient()
		if CheckForFile("versions.txt") {
			fmt.Println("Reading versions")
			versions := ReadFile("versions.txt", client)
			for version := range versions {
				attributes := strings.Split(versions[version], ",")
				var previous string
				if attributes[2] == "" {
					previous = ""
				} else {
					previous = "mcpack-" + attributes[2]
				}
				newVersion := db.Pack{
					Name:     "mcpack-" + attributes[0],
					Version:  attributes[0],
					State:    attributes[1],
					Previous: previous,
				}
				// fmt.Println(newVersion)
				db.CreateVersion(newVersion, client)
			}

		}
		if CheckForFile("mods.txt") {
			fmt.Println("Reading mods")
			mods := ReadFile("mods.txt", client)
			for mod := range mods {
				attributes := strings.Split(mods[mod], ",")
				var homepage, downloadURL string
				if attributes[2] == "" {
					homepage = ""
				} else {
					homepage = attributes[2]
				}
				if attributes[3] == "" {
					downloadURL = ""
				} else {
					downloadURL = attributes[3]
				}
				newMod := db.Mod{
					Name:        attributes[0],
					Version:     attributes[1],
					Homepage:    homepage,
					DownloadURL: downloadURL,
				}
				// fmt.Println(newMod)
				db.CreateMod(newMod, attributes[4], client)
			}
		}
		db.CheckModPacks(client)
	} else {
		http.HandleFunc("/", handler)
		log.Fatal(http.ListenAndServe(":55055", nil))
	}
}
func CheckError(e error) {
	if e != nil {
		fmt.Println(e)
	}
}
func CheckForFile(filename string) bool {
	_, error := os.Stat(filename)
	return !errors.Is(error, os.ErrNotExist)
}

func ReadFile(filename string, client *mongo.Client) []string {
	data, _ := ioutil.ReadFile(filename)
	return strings.Split(bytes.NewBuffer(data).String(), "\n")

}
